# Simple API for getting company info from czech register

License: MIT

### Requirements
1. Python 3.9
2. Poetry

### Installation
1. Clone repository && cd to the repository
2. Run `poetry install`
3. Run `poetry shell`
4. Run `uvicorn main:app --host 0.0.0.0 --port 8000`
5. Browse API docs at http://localhost:8000/docs