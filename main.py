import urllib.parse

import requests, numpy
from bs4 import BeautifulSoup
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from dateutil.easter import *
app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get('/getCompanyInfo/{icoOrName}')
async def get_company_info_by_ico(icoOrName):
    try:
        response = requests.get(f'https://wwwinfo.mfcr.cz/cgi-bin/ares/ares_es.cgi?jazyk=cz&obch_jm=&ico={int(icoOrName)}&cestina=cestina&obec=&k_fu=&maxpoc=200&ulice=&cis_or=&cis_po=&setrid=ZADNE&pr_for=&nace=&xml=1&filtr=1')
    except ValueError:
        parsed_ico_or_name = urllib.parse.quote(icoOrName, encoding='ISO 8859-2')
        response = requests.get(f'https://wwwinfo.mfcr.cz/cgi-bin/ares/ares_es.cgi?jazyk=cz&obch_jm={parsed_ico_or_name}&ico=&cestina=cestina&obec=&k_fu=&maxpoc=200&ulice=&cis_or=&cis_po=&setrid=ZADNE&pr_for=&nace=&xml=1&filtr=1', headers={
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.5',
            'Accept-Encoding': 'gzip, deflate, br',
            'Connection': 'keep-alive',
            'Upgrade-Insecure-Requests': '1',
            'Sec-Fetch-User': '?1',
            'Sec-Fetch-Site': 'same-origin',
            'Sec-Fetch-Mode': 'navigate',
            'Sec-Fetch-Dest': 'frame',
            'Referer': 'https://wwwinfo.mfcr.cz/ares/ares_es_form.html.cz',
            'Host': 'wwwinfo.mfcr.cz',
        })
    response.encoding = 'utf-8'
    xml_soup = BeautifulSoup(response.text, 'xml')
    names = xml_soup.find_all('dtt:ojm')
    addresses = xml_soup.find_all('dtt:jmn')
    icos = xml_soup.find_all('dtt:ico')

    return [{'name': name.text.strip(), 'address': address.text.strip(), 'ico': ico.text.strip()} for name, address, ico in zip(names, addresses, icos)]

@app.get('/getWorkDays/{year}/{month}')
async def get_work_days(year: int, month: int):
    if month == 12:
        next_month = 1
        next_year = year + 1
    else:
        next_month = month + 1
        next_year = year

    num_of_workdays = numpy.busday_count(f'{year}-{month:02}', f'{next_year}-{next_month:02}', holidays=[
        f'{year}-01-01',
        f'{year}-05-01',
        f'{year}-05-08',
        f'{year}-07-05',
        f'{year}-07-06',
        f'{year}-09-28',
        f'{year}-10-28',
        f'{year}-11-17',
        f'{year}-12-24',
        f'{year}-12-25',
        f'{year}-12-26'
    ])

    if int(easter(int(year)).month) == month:
        num_of_workdays -= 2
    return int(num_of_workdays)

